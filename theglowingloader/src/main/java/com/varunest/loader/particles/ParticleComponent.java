package com.varunest.loader.particles;

import ohos.app.Context;
import ohos.agp.components.Component;

public abstract class ParticleComponent extends Component {


    public ParticleComponent(Context context) {
        super(context);
    }

    public abstract void setPaintColor(int color);
}
