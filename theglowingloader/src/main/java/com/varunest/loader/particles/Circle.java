package com.varunest.loader.particles;

import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class Circle extends ParticleComponent implements Component.EstimateSizeListener,Component.DrawTask {
    private int w;
    private int h;
    private Paint paint;

    public Circle(Context context) {
        super(context);
        setEstimateSizeListener(this);
        addDrawTask(this);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        paint.setStyle(Paint.Style.FILL_STYLE);
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawCircle(w / 2, h / 2, w / 2, paint);
    }

    public void setPaintColor(int color) {
        paint.setColor(new Color(color));
        invalidate();
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        w = Component.EstimateSpec.getSize(widthEstimateConfig);
        h = Component.EstimateSpec.getSize(heightEstimateConfig);
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(w, w, Component.EstimateSpec.PRECISE),
                Component.EstimateSpec.getChildSizeWithMode(h, h, Component.EstimateSpec.PRECISE));
        return true;
    }
}
