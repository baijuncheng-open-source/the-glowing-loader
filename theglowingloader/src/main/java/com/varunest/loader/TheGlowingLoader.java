package com.varunest.loader;

import com.varunest.loader.animators.LineAnimator;
import com.varunest.loader.animators.RippleAnimator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public class TheGlowingLoader extends StackLayout implements Component.DrawTask {
    private Paint paint;
    private LineAnimator lineAnimator;
    private RippleAnimator rippleAnimator1, rippleAnimator2;
    private Configuration configuration;
    private static int solid = 0;

    public TheGlowingLoader(Context context) throws NotExistException, WrongTypeException, IOException {
        super(context);
        init();
    }

    public TheGlowingLoader(Context context, @Nullable AttrSet attrSet) throws NotExistException, WrongTypeException, IOException {
        super(context, attrSet);
        addDrawTask(this);
        getStuffFromXML(attrSet);
        init();
        setArrangeListener((i, i1, i2, i3) -> {
            initAnimationAndStart();
            return false;
        });
    }

    public TheGlowingLoader(Context context, @Nullable AttrSet attrSet, String styleName) throws NotExistException, WrongTypeException, IOException {
        super(context, attrSet, styleName);
        addDrawTask(this);
        getStuffFromXML(attrSet);
        init();
    }

    private void getStuffFromXML(AttrSet attrs) throws NotExistException, WrongTypeException, IOException {
        configuration = new Configuration();
        int Line1Color = getContext().getResourceManager().getElement(ResourceTable.Color_white).getColor();
        if (attrs.getAttr("TheGlowingLoader_theglowingloader_line_1_color").isPresent()) {
            Line1Color = attrs.getAttr("TheGlowingLoader_theglowingloader_line_1_color").get().getIntegerValue();
        }
        int Line2Color = getContext().getResourceManager().getElement(ResourceTable.Color_red).getColor();
        if (attrs.getAttr("TheGlowingLoader_theglowingloader_line_2_color").isPresent()) {
            Line2Color = attrs.getAttr("TheGlowingLoader_theglowingloader_line_2_color").get().getIntegerValue();
        }
        int RippleColor = getContext().getResourceManager().getElement(ResourceTable.Color_white).getColor();
        if (attrs.getAttr("TheGlowingLoader_theglowingloader_ripple_color").isPresent()) {
            RippleColor = attrs.getAttr("TheGlowingLoader_theglowingloader_ripple_color").get().getIntegerValue();
        }
        int Particle1Color = getContext().getResourceManager().getElement(ResourceTable.Color_yellow).getColor();
        if (attrs.getAttr("TheGlowingLoader_theglowingloader_particle_1_color").isPresent()) {
            Particle1Color = attrs.getAttr("TheGlowingLoader_theglowingloader_particle_1_color").get().getIntegerValue();
        }
        int Particle2Color = getContext().getResourceManager().getElement(ResourceTable.Color_white).getColor();
        if (attrs.getAttr("TheGlowingLoader_theglowingloader_particle_2_color").isPresent()) {
            Particle2Color = attrs.getAttr("TheGlowingLoader_theglowingloader_particle_2_color").get().getIntegerValue();
        }
        int Particle3Color = getContext().getResourceManager().getElement(ResourceTable.Color_blue).getColor();
        if (attrs.getAttr("TheGlowingLoader_theglowingloader_particle_3_color").isPresent()) {
            Particle3Color = attrs.getAttr("TheGlowingLoader_theglowingloader_particle_3_color").get().getIntegerValue();
        }
        int LineStrokeWidth = Constants.DEF_LINE_STROKE_WIDTH;
        if (attrs.getAttr("TheGlowingLoader_theglowingloader_line_stroke_width").isPresent()) {
            LineStrokeWidth = attrs.getAttr("TheGlowingLoader_theglowingloader_line_stroke_width").get().getIntegerValue();
        }
        boolean DisableShadows = false;
        if (attrs.getAttr("TheGlowingLoader_theglowingloader_disable_shadows").isPresent()) {
            DisableShadows = attrs.getAttr("TheGlowingLoader_theglowingloader_disable_shadows").get().getBoolValue();
        }
        boolean DisableRipple = false;
        if (attrs.getAttr("TheGlowingLoader_theglowingloader_disable_ripple").isPresent()) {
            DisableRipple = attrs.getAttr("TheGlowingLoader_theglowingloader_disable_ripple").get().getBoolValue();
        }
        float ShadowOpacity = Constants.DEF_SHADOW_OPACITY;
        if (attrs.getAttr("TheGlowingLoader_theglowingloader_shadow_opacity").isPresent()) {
            ShadowOpacity = attrs.getAttr("TheGlowingLoader_theglowingloader_shadow_opacity").get().getFloatValue();
        }

        configuration.setLine1Color(Line1Color);
        configuration.setLine2Color(Line2Color);
        configuration.setRippleColor(RippleColor);
        configuration.setParticle1Color(Particle1Color);
        configuration.setParticle2Color(Particle2Color);
        configuration.setParticle3Color(Particle3Color);
        configuration.setLineStrokeWidth(LineStrokeWidth);
        configuration.setDisableShadows(DisableShadows);
        configuration.setDisableRipple(DisableRipple);
        configuration.setShadowOpacity(ShadowOpacity);
    }

    public void setConfiguration(Configuration configuration) throws NotExistException, WrongTypeException, IOException {
        this.configuration = configuration;
        init();
    }

    private void init() throws NotExistException, WrongTypeException, IOException {
        if (configuration == null) {
            configuration = new Configuration(getContext());
        }
        rippleAnimator1 = new RippleAnimator(TheGlowingLoader.this, configuration);
        rippleAnimator2 = new RippleAnimator(TheGlowingLoader.this, configuration);
        lineAnimator = new LineAnimator(TheGlowingLoader.this, configuration);
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
    }

    private void startAnimation() {
        lineAnimator.start(new LineAnimator.Callback() {
            @Override
            public void onValueUpdated() {
                invalidate();
            }

            @Override
            public void startFirstCircleAnimation(float x, float y) {
                if (!configuration.isDisableRipple()) {
                    rippleAnimator1.setCircleCenter(x, y);
                    rippleAnimator1.start(TheGlowingLoader.this::invalidate, 60, 150, 270);
                }
            }

            @Override
            public void startSecondCircleAnimation(float x, float y) {
                if (!configuration.isDisableRipple()) {
                    rippleAnimator2.setCircleCenter(x, y);
                    rippleAnimator2.start(TheGlowingLoader.this::invalidate, -60, 0, Constants.INVALID_DEG);
                }
            }
        });
    }

    private void initAnimationAndStart() {
        if (solid == 0) {
            solid = 1;
            int w = getWidth();
            int h = getHeight();
            float offset = 0;
            float x1, x2, x3, x4, y1, y2, y3, y4;

            if (w > h) {
                offset = (float)((w - h) * 80 / 100);
            }

            w = w - (int) offset;
            x1 = .05f * w + offset / 2;
            y1 = h / 2 + .15f * w;
            x2 = .30f * w + offset / 2;
            y2 = h / 2 + -.12f * w;
            x3 = .70f * w + offset / 2;
            y3 = h / 2 + .27f * w;
            x4 = .95f * w + offset / 2;
            y4 = h / 2 - .02f * w;

            float circleMaxRadius = (x4 - x1) * .18f;
            rippleAnimator2.setCircleMaxRadius(circleMaxRadius);
            rippleAnimator1.setCircleMaxRadius(circleMaxRadius);
            lineAnimator.updateEdgeCoordinates(x1, x2, x3, x4, y1, y2, y3, y4);
            startAnimation();
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        rippleAnimator1.draw(canvas, paint);
        rippleAnimator2.draw(canvas, paint);
        lineAnimator.draw(canvas, paint);
    }
}

