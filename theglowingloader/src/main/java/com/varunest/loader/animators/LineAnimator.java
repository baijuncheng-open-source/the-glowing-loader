package com.varunest.loader.animators;

import com.varunest.loader.Configuration;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.MaskFilter;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

public class LineAnimator {
    private float x1, x2, x3, x4, y1, y2, y3, y4;
    private float wxa11, wxa12, wya11, wya12, wxa21, wya21, wxa22, wya22, wxa31, wya31, wxa32, wya32;
    private float rxa11, rxa12, rya11, rya12, rxa21, rya21, rxa22, rya22, rxa31, rya31, rxa32, rya32;
    private EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
    private Color mLine1Color;
    private Color mLine2Color;
    private MaskFilter mMaskFilter = new MaskFilter(70, MaskFilter.Blur.NORMAL);
    private int mLineStrokeWidth;
    private float mShadowOpacity;
    private boolean mIsDisableShadows;
    private AnimatorValue va11, va12, va21, va22, va31, va32;
    private AnimatorValue va111, va121, va211, va221, va311, va321;

    public LineAnimator(Component component, Configuration configuration) {
        mLine1Color = new Color(configuration.getLine1Color());
        mLine2Color = new Color(configuration.getLine2Color());
        mLineStrokeWidth = configuration.getLineStrokeWidth();
        mShadowOpacity = configuration.getShadowOpacity();
        mIsDisableShadows = configuration.isDisableShadows();
    }

    public void updateEdgeCoordinates(float x1, float x2, float x3, float x4, float y1, float y2, float y3, float y4) {
        this.x1 = x1;
        this.x2 = x2;
        this.x3 = x3;
        this.x4 = x4;
        this.y1 = y1;
        this.y2 = y2;
        this.y3 = y3;
        this.y4 = y4;
    }

    private void initLineCoordinates(boolean forward) {
        if (forward) {
            wxa11 = wxa12 = rxa11 = rxa12 = x1;
            wxa21 = wxa22 = rxa21 = rxa22 = x2;
            wya11 = wya12 = rya11 = rya12 = y1;
            wya21 = wya22 = rya21 = rya22 = y2;
            wxa31 = wxa32 = rxa31 = rxa32 = x3;
            wya31 = wya32 = rya31 = rya32 = y3;
        } else {
            wxa11 = wxa12 = rxa11 = rxa12 = x2;
            wxa21 = wxa22 = rxa21 = rxa22 = x3;
            wya11 = wya12 = rya11 = rya12 = y2;
            wya21 = wya22 = rya21 = rya22 = y3;
            wxa31 = wxa32 = rxa31 = rxa32 = x4;
            wya31 = wya32 = rya31 = rya32 = y4;
        }
    }

    public void draw(Canvas canvas, Paint paint) {
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setMaskFilter(null);
        paint.setStrokeWidth(mLineStrokeWidth);
        paint.setColor(mLine1Color);
        if (wxa11 != wxa12 && wya11 != wya12)
            canvas.drawLine(wxa11, wya11, wxa12, wya12, paint);
        if (wxa21 != wxa22 && wya21 != wya22)
            canvas.drawLine(wxa21, wya21, wxa22, wya22, paint);
        if (wxa31 != wxa32 && wya31 != wya32)
            canvas.drawLine(wxa31, wya31, wxa32, wya32, paint);

        paint.setColor(mLine2Color);
        if (rxa11 != rxa12 && rya11 != rya12)
            canvas.drawLine(rxa11, rya11, rxa12, rya12, paint);
        if (rxa21 != rxa22 && rya21 != rya22)
            canvas.drawLine(rxa21, rya21, rxa22, rya22, paint);
        if (rxa31 != rxa32 && rya31 != rya32)
            canvas.drawLine(rxa31, rya31, rxa32, rya32, paint);


        if (!mIsDisableShadows) {
            paint.setMaskFilter(mMaskFilter);
            paint.setStrokeWidth(2.666f * mLineStrokeWidth);
            paint.setColor(mLine1Color);
            paint.setAlpha(mShadowOpacity);
            if (wxa11 != wxa12 && wya11 != wya12)
                canvas.drawLine(wxa11, wya11 + 100f, wxa12, wya12 + 100, paint);
            if (wxa21 != wxa22 && wya21 != wya22)
                canvas.drawLine(wxa21, wya21 + 100, wxa22, wya22 + 100, paint);
            if (wxa31 != wxa32 && wya31 != wya32)
                canvas.drawLine(wxa31, wya31 + 100f, wxa32, wya32 + 100f, paint);

            paint.setColor(mLine2Color);
            paint.setAlpha(mShadowOpacity);
            if (rxa11 != rxa12 && rya11 != rya12)
                canvas.drawLine(rxa11, rya11 + 100, rxa12, rya12 + 100, paint);
            if (rxa21 != rxa22 && rya21 != rya22)
                canvas.drawLine(rxa21, rya21 + 100, rxa22, rya22 + 100, paint);
            if (rxa31 != rxa32 && rya31 != rya32)
                canvas.drawLine(rxa31, rya31 + 100, rxa32, rya32 + 100, paint);
        }
    }

    public void start(final Callback callback) {
        playForwardAnimation(new LocalCallback() {
            @Override
            public void onValueUpdated() {
                callback.onValueUpdated();
            }

            @Override
            public void startFirstCircleAnimation(float x, float y) {
                callback.startFirstCircleAnimation(x, y);
            }

            @Override
            public void startSecondCircleAnimation(float x, float y) {
                callback.startSecondCircleAnimation(x, y);
            }

            @Override
            public void onComplete() {
            }
        });
    }

    private void playForwardAnimation(final LocalCallback callback) {
        playWhiteLineForward(new LocalCallback() {
            @Override
            public void onValueUpdated() {
                callback.onValueUpdated();
            }

            @Override
            public void startFirstCircleAnimation(float x, float y) {
                callback.startFirstCircleAnimation(x, y);
            }

            @Override
            public void startSecondCircleAnimation(float x, float y) {
                callback.startSecondCircleAnimation(x, y);
            }


            @Override
            public void onComplete() {

            }
        });

        eventHandler.postTask(() -> playRedLineForward(new LocalCallback() {
            @Override
            public void onValueUpdated() {
                callback.onValueUpdated();
            }

            @Override
            public void startFirstCircleAnimation(float x, float y) {
                callback.startFirstCircleAnimation(x, y);
            }
            @Override
            public void startSecondCircleAnimation(float x, float y) {
                callback.startSecondCircleAnimation(x, y);
            }

            @Override
            public void onComplete() {
                playBackwardAnimation(callback);
            }
        }), 183);
    }

    private void playBackwardAnimation(final LocalCallback callback) {
        playWhiteLineBackward(new LocalCallback() {
            @Override
            public void onValueUpdated() {
                callback.onValueUpdated();
            }

            @Override
            public void startFirstCircleAnimation(float x, float y) {
                callback.startFirstCircleAnimation(x, y);
            }

            @Override
            public void startSecondCircleAnimation(float x, float y) {
                callback.startSecondCircleAnimation(x, y);
            }

            @Override
            public void onComplete() {

            }
        });

        eventHandler.postTask(() -> playRedLineBackward(new LocalCallback() {
            @Override
            public void onValueUpdated() {
                callback.onValueUpdated();
            }

            @Override
            public void startFirstCircleAnimation(float x, float y) {
                callback.startFirstCircleAnimation(x, y);
            }

            @Override
            public void startSecondCircleAnimation(float x, float y) {
                callback.startSecondCircleAnimation(x, y);
            }

            @Override
            public void onComplete() {
                playForwardAnimation(callback);
            }
        }), 183);
    }
    private void playWhiteLineForward(final LocalCallback callback) {
        initLineCoordinates(true);
        va11 = new AnimatorValue();
        va11.setValueUpdateListener((AnimatorValue,v) -> {
            wxa12 = x1 + (x2 - x1) * (float)Math.pow(v,3.6f);
            wya12 = y1 + (y2 - y1) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va12 = new AnimatorValue();
        va12.setValueUpdateListener((AnimatorValue,v) -> {
            wxa11 = x1 + (x2 - x1) * (float)Math.pow(v,3.6f);
            wya11 = y1 + (y2 - y1) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va21 = new AnimatorValue();
        va21.setValueUpdateListener((AnimatorValue,v) -> {
            wxa22 = x2 + (x3 - x2) * (float)Math.pow(v,3.6f);
            wya22 = y2 + (y3 - y2) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });

        va22 = new AnimatorValue();
        va22.setValueUpdateListener((AnimatorValue,v) -> {
            wxa21 = x2 + (x3 - x2) * (float)Math.pow(v,3.6f);
            wya21 = y2 + (y3 - y2) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va31 = new AnimatorValue();
        va31.setValueUpdateListener((AnimatorValue,v) -> {
            wxa32 = x3 + (x4 - x3) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            wya32 = y3 + (y4 - y3) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            callback.onValueUpdated();
        });
        va32 = new AnimatorValue();
        va32.setValueUpdateListener((AnimatorValue,v) -> {
            wxa31 = x3 + (x4 - x3) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            wya31 = y3 + (y4 - y3) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            callback.onValueUpdated();
        });

        int tw11 = 450;
        int tw21 = 143;
        int tw31 = 510;
        va11.setDuration(tw11);
        va21.setDuration(tw21);
        va31.setDuration(tw31);

        va21.setDelay(tw11);
        va31.setDelay(tw11 + tw21);

        int tw12 = 510;
        int tw22 = 165;
        int tw32 = 433;
        va12.setDuration(tw12);
        va22.setDuration(tw22);
        va32.setDuration(tw32);

        va22.setDelay(tw12);
        va32.setDelay(tw12 + tw22);

        va12.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                callback.startFirstCircleAnimation(x2,y2);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        va22.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                callback.startSecondCircleAnimation(x3,y3);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        va32.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                callback.onComplete();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

        va11.start();
        va12.start();
        va21.start();
        va22.start();
        va31.start();
        va32.start();
    }

    private void playWhiteLineBackward(final LocalCallback callback) {
        initLineCoordinates(false);
        va11 = new AnimatorValue();
        va11.setValueUpdateListener((AnimatorValue,v) -> {
            wxa32 = x4 + (x3 - x4) * (float)Math.pow(v,3.6f);
            wya32 = y4 + (y3 - y4) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va12 = new AnimatorValue();
        va12.setValueUpdateListener((AnimatorValue,v) -> {
            wxa31 = x4 + (x3 - x4) * (float)Math.pow(v,3.6f);
            wya31 = y4 + (y3 - y4) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va21 = new AnimatorValue();
        va21.setValueUpdateListener((AnimatorValue,v) -> {
            wxa22 = x3 + (x2 - x3) * (float)Math.pow(v,3.6f);
            wya22 = y3 + (y2 - y3) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va22 = new AnimatorValue();
        va22.setValueUpdateListener((AnimatorValue,v) -> {
            wxa21 = x3 + (x2 - x3) * (float)Math.pow(v,3.6f);
            wya21 = y3 + (y2 - y3) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va31 = new AnimatorValue();
        va31.setValueUpdateListener((AnimatorValue,v) -> {
            wxa12 = x2 + (x1 - x2) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            wya12 = y2 + (y1 - y2) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            callback.onValueUpdated();
        });
        va32 = new AnimatorValue();
        va32.setValueUpdateListener((AnimatorValue,v) -> {
            wxa11 = x2 + (x1 - x2) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            wya11 = y2 + (y1 - y2) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            callback.onValueUpdated();
        });

        int tw11 = 450;
        int tw21 = 143;
        int tw31 = 510;
        va11.setDuration(tw11);
        va21.setDuration(tw21);
        va31.setDuration(tw31);

        va21.setDelay(tw11);
        va31.setDelay(tw11 + tw21);

        int tw12 = 510;
        int tw22 = 165;
        int tw32 = 433;
        va12.setDuration(tw12);
        va22.setDuration(tw22);
        va32.setDuration(tw32);


        va22.setDelay(tw12);
        va32.setDelay(tw12 + tw22);

        va12.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                callback.startSecondCircleAnimation(x3,y3);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        va22.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                callback.startFirstCircleAnimation(x2,y2);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        va32.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                callback.onComplete();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

        va11.start();
        va21.start();
        va31.start();

        va12.start();
        va22.start();
        va32.start();
    }
    private void playRedLineForward(final LocalCallback callback) {
        initLineCoordinates(true);
        va111 = new AnimatorValue();
        va111.setValueUpdateListener((AnimatorValue,v) -> {
            rxa12 = x1 + (x2 - x1) * (float)Math.pow(v,3.6f);
            rya12 = y1 + (y2 - y1) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va121 = new AnimatorValue();
        va121.setValueUpdateListener((AnimatorValue,v) -> {
            rxa11 = x1 + (x2 - x1) * (float)Math.pow(v,3.6f);
            rya11 = y1 + (y2 - y1) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va211 = new AnimatorValue();
        va211.setValueUpdateListener((AnimatorValue,v) -> {
            rxa22 = x2 + (x3 - x2) * (float)Math.pow(v,3.6f);
            rya22 = y2 + (y3 - y2) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va221 = new AnimatorValue();
        va221.setValueUpdateListener((AnimatorValue,v) -> {
            rxa21 = x2 + (x3 - x2) * (float)Math.pow(v,3.6f);
            rya21 = y2 + (y3 - y2) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va311 = new AnimatorValue();
        va311.setValueUpdateListener((AnimatorValue,v) -> {
            rxa32 = x3 + (x4 - x3) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            rya32 = y3 + (y4 - y3) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            callback.onValueUpdated();
        });

        va321 = new AnimatorValue();
        va321.setValueUpdateListener((AnimatorValue,v) -> {
            rxa31 = x3 + (x4 - x3) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            rya31 = y3 + (y4 - y3) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            callback.onValueUpdated();
        });

        int tw11 = 450;
        int tw21 = 143;
        int tw31 = 510;
        va111.setDuration(tw11);
        va211.setDuration(tw21);
        va311.setDuration(tw31);

        va211.setDelay(tw11);
        va311.setDelay(tw11 + tw21);

        int tw12 = 510;
        int tw22 = 165;
        int tw32 = 433;
        va121.setDuration(tw12);
        va221.setDuration(tw22);
        va321.setDuration(tw32);


        va221.setDelay(tw12);
        va321.setDelay(tw12 + tw22);


        va321.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                callback.onComplete();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

        va111.start();
        va121.start();
        va211.start();
        va221.start();
        va311.start();
        va321.start();
    }

    private void playRedLineBackward(final LocalCallback callback) {
        initLineCoordinates(false);
        va111 = new AnimatorValue();
        va111.setValueUpdateListener((AnimatorValue,v) -> {
            rxa32 = x4 + (x3 - x4) * (float)Math.pow(v,3.6f);
            rya32 = y4 + (y3 - y4) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va121 = new AnimatorValue();
        va121.setValueUpdateListener((AnimatorValue,v) -> {
            rxa31 = x4 + (x3 - x4) * (float)Math.pow(v,3.6f);
            rya31 = y4 + (y3 - y4) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va211 = new AnimatorValue();
        va211.setValueUpdateListener((AnimatorValue,v) -> {
            rxa22 = x3 + (x2 - x3) * (float)Math.pow(v,3.6f);
            rya22 = y3 + (y2 - y3) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va221 = new AnimatorValue();
        va221.setValueUpdateListener((AnimatorValue,v) -> {
            rxa21 = x3 + (x2 - x3) * (float)Math.pow(v,3.6f);
            rya21 = y3 + (y2 - y3) * (float)Math.pow(v,3.6f);
            callback.onValueUpdated();
        });
        va311 = new AnimatorValue();
        va311.setValueUpdateListener((AnimatorValue,v) -> {
            rxa12 = x2 + (x1 - x2) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            rya12 = y2 + (y1 - y2) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            callback.onValueUpdated();
        });
        va321 = new AnimatorValue();
        va321.setValueUpdateListener((AnimatorValue,v) -> {
            rxa11 = x2 + (x1 - x2) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            rya11 = y2 + (y1 - y2) * (float)(1.0f - Math.pow((1.0f - v), 3.6f));
            callback.onValueUpdated();
        });
        int tw11 = 450;
        int tw21 = 143;
        int tw31 = 510;
        va111.setDuration(tw11);
        va211.setDuration(tw21);
        va311.setDuration(tw31);

        va211.setDelay(tw11);
        va311.setDelay(tw11 + tw21);

        int tw12 = 510;
        int tw22 = 165;
        int tw32 = 433;
        va121.setDuration(tw12);
        va221.setDuration(tw22);
        va321.setDuration(tw32);


        va221.setDelay(tw12);
        va321.setDelay(tw12 + tw22);

        va321.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                callback.onComplete();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

        va111.start();
        va121.start();
        va211.start();
        va221.start();
        va311.start();
        va321.start();
    }

    private interface LocalCallback {
        void onValueUpdated();

        void startFirstCircleAnimation(float x, float y);

        void startSecondCircleAnimation(float x, float y);

        void onComplete();
    }

    public interface Callback {
        void onValueUpdated();

        void startFirstCircleAnimation(float x, float y);

        void startSecondCircleAnimation(float x, float y);
    }
}


