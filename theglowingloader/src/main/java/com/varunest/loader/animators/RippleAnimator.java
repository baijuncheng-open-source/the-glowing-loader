package com.varunest.loader.animators;

import com.varunest.loader.Configuration;
import com.varunest.loader.Constants;
import com.varunest.loader.particles.Circle;
import com.varunest.loader.particles.ParticleComponent;
import com.varunest.loader.particles.Triangle;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.render.Canvas;
import ohos.agp.render.MaskFilter;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;

public class RippleAnimator {
    private static final int PARTICLE_TYPE_TRIANGLE = 0;
    private static final int PARTICLE_TYPE_CIRCLE = 1;

    private float circleMaxRadius;
    private float circleRadius, circleRadius2;
    private int circleStroke, circleStroke2;
    private float circleAlpha, circleAlpha2;
    private float cX, cY;
    private StackLayout component;
    private Configuration configuration;
    private Color mRippleColor;
    private float mShadowOpacity;
    private MaskFilter mMaskFilter = new MaskFilter(50, MaskFilter.Blur.NORMAL);
    private AnimatorValue va1,va2,va3;

    public RippleAnimator(StackLayout c, Configuration configuration) {
        component = c;
        this.configuration = configuration;
        mRippleColor = new Color(configuration.getRippleColor());
        mShadowOpacity = configuration.getShadowOpacity();
    }

    public void setCircleMaxRadius(float r) {
        this.circleMaxRadius = r;
    }

    public void setCircleCenter(float x, float y) {
        cX = x;
        cY = y;
    }

    public void startCircleMajor(final Callback callback) {
        va1 = new AnimatorValue();
        va1.setValueUpdateListener((AnimatorValue,v) -> {
            circleRadius = circleMaxRadius * (float)Math.pow(v,0.8f);
            circleAlpha = 1 - (float)Math.pow(v,0.8f);
            circleStroke =  (int) ((circleMaxRadius * .15f) * (1 - (float)Math.pow(v,0.8f)));
            callback.onValueUpdated();
        });
        va1.setDuration(450);
        va1.start();
    }

    public void startCircleMinor(final Callback callback) {
        va2 = new AnimatorValue();
        va2.setValueUpdateListener((AnimatorValue,v) -> {
            circleRadius2 = circleMaxRadius * .60f * (float)Math.pow(v,0.8f);
            circleAlpha2 = 1 - (float)Math.pow(v,0.8f);
            circleStroke2 =  (int) ((circleMaxRadius * .06f) * (1 - (float)Math.pow(v,0.8f)));
            callback.onValueUpdated();
        });
        va2.setDuration(380);
        va2.start();
    }

    public void startParticleAnimation(float degree, int particleType, int particleColor) {
        float length = 2 * circleMaxRadius;
        float x = (float) (cX + length * Math.cos(Math.toRadians(degree)));
        float y = (float) (cY - length * Math.sin(Math.toRadians(degree)));
        final ParticleComponent particleView;
        switch (particleType) {
            case PARTICLE_TYPE_CIRCLE:
                particleView = new Circle(component.getContext());
                break;
            case PARTICLE_TYPE_TRIANGLE:
                particleView = new Triangle(component.getContext());
                break;
            default:
                particleView = new Circle(component.getContext());
        }
        int size = (int) (circleMaxRadius * .3f);
        particleView.setLayoutConfig(new ComponentContainer.LayoutConfig(size,size));
        particleView.setPaintColor(particleColor);

        particleView.setTranslationX(cX);
        particleView.setTranslationY(cY);
        particleView.setRotation(0);
        particleView.setScaleX(0);
        particleView.setScaleY(0);
        particleView.setAlpha(1);

        component.addComponent(particleView);

        va3 = new AnimatorValue();
        va3.setValueUpdateListener((AnimatorValue,v) -> {
            float xx = cX + (x - cX) * (float)Math.pow(v,0.8f);
            float yy = cY + (y - cY) * (float)Math.pow(v,0.8f);
            float scale = (float)Math.pow(v,0.8f);
            float alpha = 1 - (float)Math.pow(v,0.8f);
            float rotation = 360 *  (float)Math.pow(v,0.8f);
            particleView.setTranslationX(xx - particleView.getLeft());
            particleView.setTranslationY(yy - particleView.getTop());
            particleView.setRotation(rotation);
            particleView.setScaleX(scale);
            particleView.setScaleY(scale);
            particleView.setAlpha(alpha);
        });
        va3.setDuration(550);

        va3.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                component.removeComponent(particleView);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        va3.start();
    }

    public void start(final Callback callback, float degree1, float degree2, float degree3) {
        startCircleMajor(callback);
        startCircleMinor(callback);
        if (degree1 != Constants.INVALID_DEG)
            startParticleAnimation(degree1, PARTICLE_TYPE_TRIANGLE, configuration.getParticle1Color());
        if (degree2 != Constants.INVALID_DEG)
            startParticleAnimation(degree2, PARTICLE_TYPE_CIRCLE, configuration.getParticle2Color());
        if (degree3 != Constants.INVALID_DEG)
            startParticleAnimation(degree3, PARTICLE_TYPE_TRIANGLE, configuration.getParticle3Color());
    }

    public void draw(Canvas canvas, Paint paint) {
        paint.setMaskFilter(null);
        paint.setStyle(Paint.Style.STROKE_STYLE);

        paint.setColor(mRippleColor);
        paint.setStrokeWidth(circleStroke);
        paint.setAlpha(circleAlpha);
        canvas.drawCircle(cX, cY, circleRadius, paint);

        if (!configuration.isDisableShadows()) {
            paint.setMaskFilter(mMaskFilter);
            paint.setStrokeWidth(.28f * circleRadius);
            paint.setAlpha(circleAlpha * mShadowOpacity);
            canvas.drawCircle(cX, cY + 100, circleRadius, paint);
        }


        paint.setMaskFilter(null);
        paint.setColor(mRippleColor);
        paint.setStrokeWidth(circleStroke2);
        paint.setAlpha(circleAlpha2);
        canvas.drawCircle(cX, cY, circleRadius2, paint);
    }

    public interface Callback {
        void onValueUpdated();
    }
}


