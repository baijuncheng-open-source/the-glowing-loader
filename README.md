# TheGlowingLoader
A highly configurable library to indicate progress and can be easily customized. You can change line stroke width, line colors, particle colors, disable several effects etc.
## Preview
![image](images/image.gif)
## demo effect
| Example A                               | Example B|
| -------------                           | -------------                       |
| ![Customized One](art/black_green.gif)  | ![Customized Two](art/grey_blue.gif)  |

## Dependency

Compile the project and copy the HAR package generated in the Build directory of the GraphView folder into the project lib folder

Add the following code inside Gradle of Entry (just Demo)

 ```
 implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
 ```

The lib is available on Maven Central, you can find it with [Gradle, please]

```xml
dependencies {
    implementation ‘com.gitee.baijuncheng-open-source:TheGlowingLoader:1.0.0’
}
```

## Usage

```xml
    <com.varunest.loader.TheGlowingLoader
        ohos:width="match_parent"
        ohos:height="match_parent"/>
```

### Attributes

```xml
<com.varunest.loader.TheGlowingLoader
        ohos:width="match_parent"
        ohos:height="match_parent"
        app:TheGlowingLoader_theglowingloader_line_1_color="#FFFF0000"
        app:TheGlowingLoader_theglowingloader_line_2_color="#FF00FF00"
        app:TheGlowingLoader_theglowingloader_line_3_color="#FF70DB93"
        app:TheGlowingLoader_theglowingloader_ripple_color="#FFB5A642"
        app:TheGlowingLoader_theglowingloader_particle_1_color="#FFFF0000"
        app:TheGlowingLoader_theglowingloader_particle_2_color="#FF00FF00"
        app:TheGlowingLoader_theglowingloader_particle_3_color="#FF70DB93"
        app:TheGlowingLoader_theglowingloader_line_stroke_width="50"
        app:TheGlowingLoader_theglowingloader_disable_shadows="false"
        app:TheGlowingLoader_theglowingloader_disable_ripple="false"
        app:TheGlowingLoader_theglowingloader_shadow_opacity=".5f"
        />
```

You can also access and modify all these attributes at runtime by getting the reference of `TheGlowingLoader` and calling its `setConfiguration` method.

## Contribution
Any contributions, large or small,features, bug fixes are welcomed and appreciated. Use pull requests, they will be thoroughly reviewed and discussed.

## License
Library falls under [Apache 2.0](LICENSE.md)

Copyright (c) 2012, Shashank Sahay

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
Neither the name of Barber nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

